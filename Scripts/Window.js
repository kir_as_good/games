window.onscroll__events = {};
window.onscroll__add = function(name, callback) {
	if (isset(window.onscroll__events[name])) return;
	window.onscroll__events[name] = callback;
};
window.onscroll__remove = function(name) {
	if (!isset(window.onscroll__events[name])) return;
	delete window.onscroll__events[name];
};
window.onscroll__get = function() {
	return window.pageYOffset || document.documentElement.scrollTop;
};
window.onscroll = function() {
	let scrolled = window.onscroll__get();
	window.onscroll__current = scrolled;
	for (let i in window.onscroll__events) window.onscroll__events[i](scrolled);
};
window.onscroll__current = window.onscroll__get();

window.onresize__events = {};
window.onresize__add = function(name, callback) {
	if (isset(window.onresize__events[name])) return;
	window.onresize__events[name] = callback;
};
window.onresize__remove = function(name) {
	if (!isset(window.onresize__events[name])) return;
	delete window.onresize__events[name];
};
window.onresize__get = function() {
	return {
		width: window.innerWidth,
		height: window.innerHeight
	};
};
window.onresize = function() {
	let size = window.onresize__get();
	window.onresize__current = size;
	for (let i in window.onresize__events) window.onresize__events[i](size);
};
window.onresize__current = window.onresize__get();

window.onload__events = {};
window.onload__add = function(name, callback) {
	if (isset(window.onload__events[name])) return;
	window.onload__events[name] = callback;
};
window.onload__remove = function(name) {
	if (!isset(window.onload__events[name])) return;
	delete window.onload__events[name];
};
window.onload = function() {
	for (let i in window.onload__events) window.onload__events[i]();
};

window.onunload__events = {};
window.onunload__add = function(name, callback) {
	if (isset(window.onunload__events[name])) return;
	window.onunload__events[name] = callback;
};
window.onunload__remove = function(name) {
	if (!isset(window.onunload__events[name])) return;
	delete window.onunload__events[name];
};
window.onunload = function() {
	for (let i in window.onunload__events) window.onunload__events[i]();
};

window.onbeforeunload__events = {};
window.onbeforeunload__add = function(name, callback) {
	if (isset(window.onbeforeunload__events[name])) return;
	window.onbeforeunload__events[name] = callback;
};
window.onbeforeunload__remove = function(name) {
	if (!isset(window.onbeforeunload__events[name])) return;
	delete window.onbeforeunload__events[name];
};
window.onbeforeunload = function() {
	for (let i in window.onbeforeunload__events) window.onbeforeunload__events[i]();
};