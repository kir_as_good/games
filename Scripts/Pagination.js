function Pagination_(parent, element, onShowPage, onInit, tabsAllowed, autoPaged) {
	let self = this;

	this.parent = parent;
	this.tabsAllowed = (typeof tabsAllowed == 'undefined'?3:tabsAllowed);
	this.autoPaged = (typeof autoPaged == 'undefined'?false:autoPaged);
	this.onInit = onInit;
	this.onShowPage = onShowPage;
	this.element = element;
	this.element.classList.add('Pagination');

	if (typeof this.parent.settings=='undefined') {
		this.parent.settings = {};
	}
	if (typeof this.parent.settings.pageIndex=='undefined') this.parent.settings.pageIndex = 1;
	if (typeof this.parent.settings.totalPagesCount=='undefined') this.parent.settings.totalPagesCount = 1;

	this.render = function () {
		if (self.autoPaged) {
			self.element.style.visibility = 'hidden';
			self.element.style.height = 'calc(var(--s-bs) * 4)';
			self.element.style.background = 'background: url("data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22200px%22%20%20height%3D%22200px%22%20%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20100%20100%22%20preserveAspectRatio%3D%22xMidYMid%22%20class%3D%22lds-ripple%22%20style%3D%22background%3A%20none%3B%22%3E%3Ccircle%20cx%3D%2250%22%20cy%3D%2250%22%20r%3D%2228.407%22%20fill%3D%22none%22%20ng-attr-stroke%3D%22%7B%7Bconfig.c1%7D%7D%22%20ng-attr-stroke-width%3D%22%7B%7Bconfig.width%7D%7D%22%20stroke%3D%22%238cd0e5%22%20stroke-width%3D%222%22%3E%3Canimate%20attributeName%3D%22r%22%20calcMode%3D%22spline%22%20values%3D%220%3B40%22%20keyTimes%3D%220%3B1%22%20dur%3D%221%22%20keySplines%3D%220%200.2%200.8%201%22%20begin%3D%22-0.5s%22%20repeatCount%3D%22indefinite%22%3E%3C%2Fanimate%3E%3Canimate%20attributeName%3D%22opacity%22%20calcMode%3D%22spline%22%20values%3D%221%3B0%22%20keyTimes%3D%220%3B1%22%20dur%3D%221%22%20keySplines%3D%220.2%200%200.8%201%22%20begin%3D%22-0.5s%22%20repeatCount%3D%22indefinite%22%3E%3C%2Fanimate%3E%3C%2Fcircle%3E%3Ccircle%20cx%3D%2250%22%20cy%3D%2250%22%20r%3D%226.15459%22%20fill%3D%22none%22%20ng-attr-stroke%3D%22%7B%7Bconfig.c2%7D%7D%22%20ng-attr-stroke-width%3D%22%7B%7Bconfig.width%7D%7D%22%20stroke%3D%22%23376888%22%20stroke-width%3D%222%22%3E%3Canimate%20attributeName%3D%22r%22%20calcMode%3D%22spline%22%20values%3D%220%3B40%22%20keyTimes%3D%220%3B1%22%20dur%3D%221%22%20keySplines%3D%220%200.2%200.8%201%22%20begin%3D%220s%22%20repeatCount%3D%22indefinite%22%3E%3C%2Fanimate%3E%3Canimate%20attributeName%3D%22opacity%22%20calcMode%3D%22spline%22%20values%3D%221%3B0%22%20keyTimes%3D%220%3B1%22%20dur%3D%221%22%20keySplines%3D%220.2%200%200.8%201%22%20begin%3D%220s%22%20repeatCount%3D%22indefinite%22%3E%3C%2Fanimate%3E%3C%2Fcircle%3E%3C%2Fsvg%3E") no-repeat center transparent';
			self.element.style.backgroundSize = 'calc(var(--s-bs) * 3)';
		} else {
			removeChildren(self.element);
			let x = 1,
				item,
				subItem;
			if (self.parent.settings.totalPagesCount>1) {
				item = createElement('li');
				item.className = ['Pagination__item', (self.parent.settings.pageIndex > 1 ? '' : '__disabled')].join(' ');
				subItem = createElement('a');
				if (self.parent.settings.pageIndex > 1) {
					subItem.addEventListener('click', function(e){
						e.preventDefault();
						self.prev_page();
						return false;
					});
				}
				subItem.innerText = LL.get('Назад');
				item.appendChild(subItem);
				self.element.appendChild(item);

				while (x <= self.parent.settings.totalPagesCount) {
					if (x === self.parent.settings.pageIndex || x === self.parent.settings.totalPagesCount || x === 1 || (x > self.parent.settings.pageIndex && x <= self.parent.settings.pageIndex + self.tabsAllowed) || (x < self.parent.settings.pageIndex && x >= self.parent.settings.pageIndex - self.tabsAllowed)) {
						item = createElement('li');
						item.className = ['Pagination__item', (x === self.parent.settings.pageIndex ? '__active' : '')].join(' ');
						subItem = createElement('a');
						if (x !== self.parent.settings.pageIndex) {
							subItem._page = x;
							subItem.addEventListener('click', function(e){
								e.preventDefault();
								self.show_page(this._page);
								return false;
							});
						}
						subItem.innerText = x;
						item.appendChild(subItem);
						self.element.appendChild(item);
					} else if (x === (self.parent.settings.pageIndex + (self.tabsAllowed + 1)) || x === (self.parent.settings.pageIndex - (self.tabsAllowed + 1))) {
						item = createElement('li');
						item.className = 'Pagination__item';
						subItem = createElement('a');
						subItem.innerText = '...';
						subItem.addEventListener('click', function(e){
							e.preventDefault();
							return false;
						});
						item.appendChild(subItem);
						self.element.appendChild(item);
					}
					x++;
				}
				if (self.parent.settings.totalPagesCount>10) {
					item = createElement('li');
					item.className = 'Pagination__item';
					subItem = createElement('input');
					subItem.type = 'number';
					subItem.setAttribute('min', 1);
					subItem.setAttribute('max', self.parent.settings.totalPagesCount);
					subItem.value = self.parent.settings.pageIndex;
					subItem.addEventListener('change', function(e){
						e.preventDefault();
						let v = parseInt(this.value);
						if (v>1&&v<=self.parent.settings.totalPagesCount) {
						} else if (v>1) {
							v = self.parent.settings.totalPagesCount;
						} else {
							v = 1;
						}
						self.show_page(v);
						return false;
					});
					item.appendChild(subItem);
					self.element.appendChild(item);
				}

				item = createElement('li');
				item.className = ['Pagination__item', (self.parent.settings.pageIndex < self.parent.settings.totalPagesCount ? '' : '__disabled')].join(' ');
				subItem = createElement('a');
				if (self.parent.settings.pageIndex < self.parent.settings.totalPagesCount) {
					subItem.addEventListener('click', function(e){
						e.preventDefault();
						self.next_page();
						return false;
					});
				}
				subItem.innerText = LL.get('Вперед');
				item.appendChild(subItem);
				self.element.appendChild(item);
			}
		}
	};
	this.next_page = function (e) {
		let x = self.parent.settings.pageIndex + 1;
		x = (x > self.parent.settings.totalPagesCount ? self.parent.settings.totalPagesCount : x);
		self.show_page(x);
	};
	this.prev_page = function (e) {
		let x = self.parent.settings.pageIndex - 1;
		x = (x < 1 ? 1 : x);
		self.show_page(x);
	};
	this.show_page = function (pageNumber) {
		self.parent.settings.pageIndex = pageNumber;
		self.onShowPage__action();
		self.render();
	};
	this.reload = function() {
		self.onShowPage__action();
	};
	this.onShowPage__action = function() {
		self.onShowPage(self.parent.settings.pageIndex, function(){
			self.element.style.visibility = 'visible';
		}, function(){
			self.element.style.visibility = 'hidden';
		});
	};
	this.onScroll = function(scrolled){
		if (scrolled>=(self.element.offsetTop - 200 - window.innerHeight) && !self.parent.settings.load) {
			self.next_page();
		}
	};
	this.init = function() {
		if (typeof self.onInit == 'function') self.onInit(self.parent.settings.pageIndex);
		if (self.autoPaged) {
			window.onscroll__add(['Pagination',self.element.id].join('__'), self.onScroll);
		}
	};
	this.init();
}