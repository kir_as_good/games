function Tron_() {
	let self = this;

	this.size = 51;
	this.field = {};
	this.elements = {};
	this.players = {
		A: {},
		B: {}
	};
	this.speed = 10000;
	this.createField = function() {
		let x=self.size,
			y,
			element,
			fragment = document.createDocumentFragment();
		while (x>0) {
			self.field[x] = {};
			y = self.size;
			while (y>0) {
				element = createElement({
					tag: 'div',
					style: {
						width: (100/self.size)+'%'
					},
					attributes: {x: x, y: y}
				});
				self.field[x][y] = {
					player: false,
					x: x,
					y: y,
					element: element
				};
				element.model = self.field[x][y];
				fragment.appendChild(element);
				y--;
			}
			x--;
		}
		self.elements.body.appendChild(fragment);
	};
	this.Start = function() {
		self.createField();
		self.players.A = new Tron_player_({
			Game: self,
			playerName: 'A',
			human: true,
			headPosition: {
				x: parseInt(self.size / 2) + 1,
				y: parseInt(self.size / 100 * 75) + 1
			},
			order: 'right'
		});
		self.players.B = new Tron_player_({
			Game: self,
			playerName: 'B',
			human: false,
			headPosition: {
				x: parseInt(self.size / 2) + 1,
				y: parseInt(self.size / 100 * 25) + 1
			},
			order: 'left'
		});
		self.players.C = new Tron_player_({
			Game: self,
			playerName: 'C',
			human: false,
			headPosition: {
				x: parseInt(self.size / 100 * 25) + 1,
				y: parseInt(self.size / 100 * 75) + 1
			},
			order: 'up'
		});
		self.players.D = new Tron_player_({
			Game: self,
			playerName: 'D',
			human: false,
			headPosition: {
				x: parseInt(self.size / 100 * 75) + 1,
				y: parseInt(self.size / 100 * 25) + 1
			},
			order: 'down'
		});
		self.cycle();
	};
	this.next = function() {
		if (self.gameOver) return;
		self.players.A.next();
		self.players.B.next();
		self.players.C.next();
		self.players.D.next();
		if (!self.players.A.live&&!self.players.B.live&&!self.players.C.live&&!self.players.D.live) self.Stop();
		self.cycle();
	};
	this.cycle = function() {
		setTimeout(self.next, (self.speed/100));
		self.speed--;
	};
	this.gameOver = false;
	this.getLivePlayers = function() {
		let live = [];
		if (self.players.A.live) live.push('A');
		if (self.players.B.live) live.push('B');
		if (self.players.C.live) live.push('C');
		if (self.players.D.live) live.push('D');
		return live;
	};
	this.Stop = function() {
		self.gameOver = true;
		let max = 0,
			livePlayers = self.getLivePlayers(),
			winner = (livePlayers.length?livePlayers[0]:'');
		for (let player in self.players) {
			if (max<self.players[player].size) {
				max = self.players[player].size;
				winner = player;
			}
		}
		if (winner === 'A') {
			self.Win(max);
		} else {
			self.Loose(max);
		}
	};
	this.Win = function(max) {
		let msg = createElement({tag: 'span', innerHTML: ['You Win. You have ',self.players.A.size,' points.'].join(''), style: {background:'var(--c-green)'}});
		self.elements.body.appendChild(msg);
	};
	this.Loose = function(max) {
		let msg = createElement({tag: 'span', innerHTML: ['You Loose. You have ',self.players.A.size,' points. Max: ',max,' points.'].join(''), style: {background:'var(--c-red)'}});
		self.elements.body.appendChild(msg);
	};
	this.init = function() {
		self.elements.container = createElement({
			className: 'Tron'
		});
		self.elements.header = createElement({
			className: 'Tron__header'
		});
		self.elements.container.appendChild(self.elements.header);
		self.elements.body = createElement({
			className: 'Tron__body'
		});
		self.elements.container.appendChild(self.elements.body);
		let content__container = document.getElementById('content__container');
		removeChildren(content__container);
		content__container.appendChild(self.elements.container);
		self.Start();
	};
	this.init();
}

function Tron_player_(model) {
	let self = this,
		color;
	this.Game = model.Game;
	this.playerName = model.playerName;
	this.human = model.human;
	switch (this.playerName) {
		case 'A':
			color = 'var(--c-red)';
		break;
		case 'B':
			color = 'var(--c-azure)';
		break;
		case 'C':
			color = 'var(--c-green)';
		break;
		case 'D':
			color = 'var(--c-gold)';
		break;
	}
	this.color = color;
	this.headPosition = model.headPosition;
	this.order = model.order;
	this.size = 1;
	this.live = true;
	this.points = [];
	this.setPoint = function(position) {
		let success = false;
		if (isset(self.Game.field[position.x])&&isset(self.Game.field[position.x][position.y])&&!self.Game.field[position.x][position.y].player) {
			self.Game.field[position.x][position.y].player = self.playerName;
			self.Game.field[position.x][position.y].element.style.background = self.color;
			self.Game.field[position.x][position.y].element.style.boxShadow = '0 0 calc(var(--s-bs)/ 2) '+self.color;
			self.headPosition = position;
			self.points.push(self.Game.field[position.x][position.y]);
			self.size++;
			success=true;
		} else {
			self.die();
		}
	};
	this.next = function() {
		if (self.human) {
			return self.move(self.order);
		} else {
			return self.automove();
		}
	};
	this.automove = function() {
		let impossible = [],
			possible = ['left','right','up','down'],
			superpossible = [],
			i;
		for (i=(possible.length-1);i>=0;i--) {
			if (!self.checkMove(possible[i], self.headPosition)) {
				impossible.push(possible[i]);
			}
		}
		superpossible = possible.diff(impossible);
		if (superpossible.length) {
			let order = superpossible[randomInteger(0, (superpossible.length-1))];
			return self.move(order);
		} else {
			return self.move(self.order);
		}
	};
	this.checkMove = function(order, position) {
		if (self.order === order) return false;
		switch (order) {
			case 'left':
				if (position.y===self.Game.size) return false;
				if (self.Game.field[position.x][(position.y+1)].player) return false;
			break;
			case 'right':
				if (position.y===1) return false;
				if (self.Game.field[position.x][(position.y-1)].player) return false;
			break;
			case 'up':
				if (position.x===self.Game.size) return false;
				if (self.Game.field[(position.x+1)][position.y].player) return false;
			break;
			case 'down':
				if (position.x===1) return false;
				if (self.Game.field[(position.x-1)][position.y].player) return false;
			break;
		}
		return true;
	};
	this.move = function(arrow) {
		if (!self.live) return;
		let currentPosition = Object.assign({}, self.headPosition),
			changed = false;
		switch (arrow) {
			case 'left':
				currentPosition.y++;
				changed = true;
			break;
			case 'right':
				currentPosition.y--;
				changed = true;
			break;
			case 'up':
				currentPosition.x++;
				changed = true;
			break;
			case 'down':
				currentPosition.x--;
				changed = true;
			break;
			default:
				self.die();
			break;
		}
		if (changed) {
			self.Game.field[self.headPosition.x][self.headPosition.y].element.style.opacity = '0.2';
			self.Game.field[self.headPosition.x][self.headPosition.y].element.style.boxShadow = '0 0 calc(var(--s-bs) / 4) '+self.color;
			return self.setPoint(currentPosition);
		}
		return false;
	};
	this.die = function() {
		self.live=false;
		for (let i=self.points.length-1;i>=0;i--) {
			self.points[i].element.style.opacity = '';
			self.points[i].element.style.boxShadow = '';
			self.points[i].element.style.background = '';
			self.points[i].player = false;
		}
	};
	this.init = function() {
		self.setPoint(self.headPosition);
		if (self.human) {
			document.addEventListener('keydown', function(e){
				let order = self.order;
				switch (e.keyCode) {
					case 37:
						order = 'left';
					break;
					case 38:
						order = 'up';
					break;
					case 39:
						order = 'right';
					break;
					case 40:
						order = 'down';
					break;
				}
				if (self.order===order) return;
				self.order=order;
			});
			self.buttons = {
				left: createElement({
						tag: 'button',
						className: 'Tron__button __left',
						hidden: true,
						events: {
							click: function(e) {
								e.preventDefault();
								self.move('left');
								self.buttons.left.hidden = true;
								self.buttons.right.hidden = true;
								self.buttons.up.hidden = false;
								self.buttons.down.hidden = false;
								return false;
							}
						}
					}),
				right: createElement({
						tag: 'button',
						className: 'Tron__button __right',
						hidden: true,
						events: {
							click: function(e) {
								e.preventDefault();
								self.move('right');
								self.buttons.left.hidden = true;
								self.buttons.right.hidden = true;
								self.buttons.up.hidden = false;
								self.buttons.down.hidden = false;
								return false;
							}
						}
					}),
				up: createElement({
						tag: 'button',
						className: 'Tron__button __up',
						hidden: false,
						events: {
							click: function(e) {
								e.preventDefault();
								self.move('up');
								self.buttons.left.hidden = false;
								self.buttons.right.hidden = false;
								self.buttons.up.hidden = true;
								self.buttons.down.hidden = true;
								return false;
							}
						}
					}),
				down: createElement({
						tag: 'button',
						className: 'Tron__button __down',
						hidden: false,
						events: {
							click: function(e) {
								e.preventDefault();
								self.move('down');
								self.buttons.left.hidden = false;
								self.buttons.right.hidden = false;
								self.buttons.up.hidden = true;
								self.buttons.down.hidden = true;
								return false;
							}
						}
					})
			};
			self.Game.elements.body.appendChild(self.buttons.left);
			self.Game.elements.body.appendChild(self.buttons.right);
			self.Game.elements.body.appendChild(self.buttons.up);
			self.Game.elements.body.appendChild(self.buttons.down);
		}
	};
	this.init();
}