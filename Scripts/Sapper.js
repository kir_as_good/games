function Sapper_() {
    let self = this;
    this.container = null;
    this.height = 20;
    this.width = 50;
    this.bombsIndex = 4;
    this.items = {};
    this.title = null;
    this.render = {
        container: function() {
            self.container = createElement({
                tag: 'div',
                id: 'container'
            });
            self.title = createElement({
                tag: 'h1',
                innerText: LL.get('Sapper')
            });
            let newGameBtn = createElement({
                tag: 'button',
                innerText: LL.get('New game'),
                className: 'btn __goldGradient',
                events: {
                    click: function(e){
                        e.preventDefault();
                        window.location.reload();
                        return false;
                    }
                }
            });
            self.title.appendChild(newGameBtn);
            self.container.appendChild(self.title);
        },
        item: function(x,y,hasBomb,is_hidden,currentState) {
            let item = createElement('div'),
                brea = createElement('div'),
                cont = createElement('div');
            item.className = 'item';
            item.style.width = (100/self.width)+'%';
            item.x = x;
            item.y = y;
            item.closestBombs = 0;
            item.is_hidden = (isset(is_hidden)?is_hidden:true);
            item.hasBomb = (isset(hasBomb)?hasBomb:(!randomInteger(0, self.bombsIndex)));
            item.hasClosestBombs = false;
            brea.style.background = (isset(currentState)&&currentState?'var(--c-red)':'var(--c-gray__1)');
            item.brea = brea;
            item.appendChild(brea);
            cont.innerHTML = (item.hasBomb?'':'');
            cont.style.visibility = 'hidden';
            if (item.hasBomb) {
                item.style.color = 'var(--c-red)'
            }
            item.cont = cont;
            item.appendChild(cont);
            item.onClick = function(e) {
                e.preventDefault();
                this.show();
                if (this.hasBomb) {
                    self.Stop(false);
                } else {
                    var items = self.getClosestItems(this);
                    items = self.explodeEmptyItems(items,this);
                    self.showItems(items);
                }
                this.removeEventListener('click', this.onClick);
                this.removeEventListener('contextmenu', this.onContextMenu);
                this.addEventListener('click', function(e){e.preventDefault();return false;});
                this.addEventListener('contextmenu', function(e){e.preventDefault();return false;});
                if (!self.checkUnfindBombs()&&!self.checkFakeFindBombs()) {
                    self.Stop(true);
                }
                return false;
            };
            item.currentState = (isset(currentState)?currentState:false);
            item.onContextMenu = function(e){
                e.preventDefault();
                if (!item.currentState) {
                    this.currentState = true;
                    this.brea.style.background = 'var(--c-red)';
                    if (!self.checkUnfindBombs()&&!self.checkFakeFindBombs()) {
                        self.Stop(true);
                    }
                } else {
                    this.currentState = false;
                    this.brea.style.background = 'var(--c-gray__1)';
                }
                return false;
            };
            item.show = function() {
                this.brea.style.opacity = 0;
                this.cont.style.visibility = 'visible';
                this.is_hidden = false;
            };
            item.addEventListener('click', item.onClick);
            item.addEventListener('contextmenu', item.onContextMenu);
            if (!isset(self.items[x])) self.items[x] = {};
            if (isset(is_hidden)&&!is_hidden) item.show();
            self.items[x][y] = item;
            return item;
        },
        ints: function() {
            for (let x in self.items) {
                for (let y in self.items[x]) {
                    if (self.items[x][y].hasBomb) continue;
                    let closest = self.getClosestItems(self.items[x][y]),
                        bombs = 0;
                    closest.push(self.items[x][y]);
                    for (let i=(closest.length-1);i>=0;i--) {
                        if (closest[i].hasBomb) bombs++;
                    }
                    if (bombs) {
                        self.items[x][y].cont.innerHTML = bombs;
                        self.items[x][y].hasClosestBombs = true;
                    }
                }
            }
        }
    };
    this.explodeEmptyItems = function(items, item) {
        let newItems = [];
        for (let i=(items.length-1);i>=0;i--) {
            if (items[i].hasClosestBombs||items[i].hasBomb||(items[i].x===item.x&&items[i].y===item.y)) {
                newItems.push(items[i]);
            } else {
                let closestItems = self.getClosestItems(items[i]);
                closestItems.push(items[i]);
                for (let ii=(closestItems.length-1);ii>=0;ii--) {
                    if (!closestItems[ii].hasClosestBombs&&!closestItems[ii].hasBomb) {
                        newItems.push(closestItems[ii]);
                    }
                }
            }
        }
        return newItems;
    };
    this.getClosestItems = function(item) {
        if (item.hasClosestBombs) return [];
        let items = [], filtered = [];
        items.push(self.items[item.x][item.y - 1]);
        items.push(self.items[item.x][item.y + 1]);
        if (item.x!==1) {
            items.push(self.items[item.x - 1][item.y]);
            items.push(self.items[item.x - 1][item.y - 1]);
            items.push(self.items[item.x - 1][item.y + 1]);
        }
        if (item.x!==self.height) {
            items.push(self.items[item.x + 1][item.y]);
            items.push(self.items[item.x + 1][item.y - 1]);
            items.push(self.items[item.x + 1][item.y + 1]);
        }
        filtered = items.filter(function (el) {
          return el != null;
        });
        return filtered;
    };
    this.checkUnfindBombs = function() {
        let hasUnfindBombs = false;
        for (let x in self.items) {
            for (let y in self.items[x]) {
                if (self.items[x][y].hasBomb&&!self.items[x][y].currentState) hasUnfindBombs = true;
            }
        }
        return hasUnfindBombs;
    };
    this.checkFakeFindBombs = function() {
        let hasFakeFindBombs = false;
        for (let x in self.items) {
            for (let y in self.items[x]) {
                if (!self.items[x][y].hasBomb&&self.items[x][y].currentState) hasFakeFindBombs = true;
            }
        }
        return hasFakeFindBombs;
    };
    this.showItems = function(items) {
        for (let i=0;i<items.length;i++) {
            if (!items[i].is_hidden||items[i].hasBomb) continue;
            items[i].show();
        }
    };
    this.Start = function() {
        self.title.hidden = true;
        for (let x=1;x<=self.height;x++) {
            for (let y=1;y<=self.width;y++) {
                self.container.appendChild(self.render.item(x, y));
            }
        }
        self.render.ints();
    };
    this.Stop = function(win) {
        self.title.hidden = false;
        for (let x in self.items) {
            for (let y in self.items[x]) {
                self.items[x][y].show();
            }
        }
        if (win) {
            self.Win();
        } else {
            self.Loose();
        }
    };
    this.Win = function() {
        self.container.appendChild(createElement({
            className: 'finalMsg',
            innerText: 'Youre Win!',
            style: {
                background: 'var(--c-green)'
            }
        }));
    };
    this.Loose = function() {
        self.container.appendChild(createElement({
            className: 'finalMsg',
            innerText: 'Youre Loose!',
            style: {
                background: 'var(--c-red)'
            }
        }));
    };
    this.Form = function() {
        let body = document.getElementById('content__container'),
            prev = document.getElementById('container');
        if (prev!=null) body.removeChild(prev);
        if (self.container!=null) self.container = null;
        self.render.container();
        let pre_width = createElement({
                tag: 'label',
                className: 'formControl __number'
            }),
            width = createElement({
                tag: 'input',
                attributes: {
                    type: 'number',
                    value: self.width,
                },
                events: {
                    input: function(){
                        self.width = parseInt(this.value);
                    }
                }
        });
        pre_width.appendChild(width);
        self.container.appendChild(pre_width);
        let pre_height = createElement({
                tag: 'label',
                className: 'formControl __number'
            }),
            height = createElement({
            tag: 'input',
            attributes: {
                type: 'number',
                value: self.height,
            },
            events: {
                input: function(){
                    self.height = parseInt(this.value);
                }
            }
        });
        pre_height.appendChild(height);
        self.container.appendChild(pre_height);
        let pre_bombsIndex = createElement({
                tag: 'label',
                className: 'formControl __number'
            }),
            bombsIndex = createElement({
            tag: 'input',
            attributes: {
                type: 'number',
                value: self.bombsIndex,
            },
            events: {
                input: function(){
                    self.bombsIndex = parseInt(this.value);
                }
            }
        });
        pre_bombsIndex.appendChild(bombsIndex);
        self.container.appendChild(pre_bombsIndex);
        let subm = createElement({
            tag: 'button',
            innerText: LL.get('Start'),
            className: 'btn __goldGradient __block',
            events: {
                click: function(){
                    self.container.removeChild(pre_width);
                    self.container.removeChild(pre_height);
                    self.container.removeChild(pre_bombsIndex);
                    self.container.removeChild(subm);
                    self.Start();
                    self.Save();
                }
            }
        });
        self.container.appendChild(subm);
        body.appendChild(self.container);
    };
    this.Save = function() {
        let currentGame = {};
        currentGame.width = self.width;
        currentGame.height = self.height;
        currentGame.bombsIndex = self.bombsIndex;
        localStorage.setItem('Sapper', JSON.stringify(currentGame));
    };
    this.Load = function() {
        let currentGame,
            check = localStorage.getItem('Sapper');
        if (check!=null) {
            currentGame = JSON.parse(check);
            self.width = currentGame.width;
            self.height = currentGame.height;
            self.bombsIndex = currentGame.bombsIndex;
        }
        self.Form();
    };
    this.Load();
}