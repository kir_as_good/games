function UrlController_() {
	let self = this;
	this.url = window.location.href;
	this.baseUrl = (LL.current==='ru'?[]:[LL.current]);
	this.parse = function(array) {
		return ['/',self.baseUrl.slice(0).concat(array).join('/')].join('');
	};
	this.change = function(url) {
		history.pushState(null, null, url);
	};
	this.parseAndChange = function(array) {
		self.change(self.parse(array));
	};
}