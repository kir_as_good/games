window.formControl = {
	hasSuccess: function(a) {
		let input = (isset(this.formControl)?this.formControl:this);
		if (a) {
			input.container.classList.add('__hasSuccess');
		} else {
			input.container.classList.remove('__hasSuccess');
		}
	},
	hasError: function(a) {
		let input = (isset(this.formControl)?this.formControl:this);
		if (a) {
			input.container.classList.add('__hasError');
		} else {
			input.container.classList.remove('__hasError');
		}
	},
	setActive: function() {
		let input = (isset(this.formControl)?this.formControl:this);
		input.input.setAttribute('checked', true);
		input.input.checked = true;
		input.container.classList.add('__active');
	},
	unsetActive: function() {
		let input = (isset(this.formControl)?this.formControl:this);
		input.input.removeAttribute('checked');
		input.input.checked = false;
		input.container.classList.remove('__active');
	},
	setAvailable: function() {
		let input = (isset(this.formControl)?this.formControl:this);
		if (!input.container.classList.contains('__available')) input.container.classList.add('__available');
	},
	unsetAvailable: function() {
		let input = (isset(this.formControl)?this.formControl:this);
		if (input.container.classList.contains('__available')) input.container.classList.remove('__available');
	},
	toggleActive: function() {
		let input = (isset(this.formControl)?this.formControl:this);
		if (input.container.classList.contains('__active')) {
			input.container.classList.remove('__active');
		} else {
			if (input.input.type==='radio') {
				var radios = document.querySelectorAll(['input[type="radio"][name="',input.input.name,'"]'].join(''));
				for (i=(radios.length - 1);i>=0;i--) radios[i].formControl.container.classList.remove('__active');
			} else if (input.input.type==='checkbox') {
				var checkboxes = document.querySelectorAll(['input[type="checkbox"][name="',input.input.name,'"]'].join(''));
				if (input.input.getAttribute('primary')) {
					for (i=(checkboxes.length - 1);i>=0;i--) {
						checkboxes[i].formControl.unsetActive();
					}
				} else {
					for (i=(checkboxes.length - 1);i>=0;i--) {
						if (checkboxes[i].getAttribute('primary')) {
							checkboxes[i].formControl.unsetActive();
						}
					}
				}
				input.input.setAttribute('checked', true);
			}
			input.container.classList.add('__active');
		}
	},
	badgeUpdate: function(text) {
		let input = (isset(this.formControl)?this.formControl:this);
		input.badge.innerText = text;
	},
	badgeHelperUpdate: function(innerHTML, hideTimer) {
		let input = (isset(this.formControl)?this.formControl:this),
			helper;
		if (!input.badge) return;
		helper = input.badge.querySelector('.formControl__badge__helper');
		if (helper) {
			helper.innerHTML = innerHTML;
			helper.hidden = false;
		} else {
			helper = formControl.renderBadgeHelper(innerHTML);
			input.badge.appendChild(helper);
		}
		if (hideTimer) {
			setTimeout(function(){
				helper.hidden = true;
			}, hideTimer);
		}
	},
	labelUpdate: function(text) {
		input.label.innerText = text;
	},
	renderBadgeHelper: function(innerHTML) {
		return createElement({
			tag: 'span',
			className: 'formControl__badge__helper',
			innerHTML: innerHTML
		});
	},
	renderControl: function(model) {
		/*
		model = {
			name: 'name',		// String
			value: 'value', 	// String
			label: 'Some label',// String
			badge: 20, 			// Integer
			active: true,		// false||true
			hasError: false,	// false||true
			hasSuccess: false,	// false||true
			primary: false, 	// false||true
			box: false, 		// false||true||half
			size: false, 		// false||xs||sm||md||lg||xl
			type: 'checkbox', 	// text||email||number||date||range||radio||checkbox||select||textarea||password||color||datetime-local||file||image||month||tel||time||url||week
			attributes: { 		// Object
				// min: 0,
				// max: 10,
				// step: 1
			}
		};
		*/
		if (!isset(model.attributes)) model.attributes = {};
		model.attributes.name = model.name;
		if (isset(model.primary)&&model.primary) model.attributes.primary = true;
		if (!isset(model.events)) model.events = {};
		let inputTag,
			inputType,
			inputInnerHTML,
			children = [];
		switch (model.type) {
			case 'text': case 'email': case 'number': case 'date': case 'range':
			case 'password': case 'color': case 'datetime-local': case 'file':
			case 'image': case 'month': case 'tel': case 'time': case 'url': case 'week':
				inputTag = 'input';
				model.attributes.value = (isset(model.value)?model.value:'');
				model.attributes.type = model.type;
				inputType = model.type;
			break;
			case 'radio': case 'checkbox':
				inputTag = 'input';
				model.attributes.value = (isset(model.value)?model.value:'');
				model.attributes.type = model.type;
				inputType = model.type;
				if (isset(model.active)&&model.active) model.attributes.checked = true;
			break;
			case 'select':
				inputTag = 'select';
				inputInnerHTML = [];
				for (let index in model.choices) {
					inputInnerHTML.push(['<option value="',index,'" ',(index===model.value?'selected':''),'>',model.choices[index],'</option>'].join(''));
				}
				inputInnerHTML = inputInnerHTML.join('');
				inputType = 'select';
			break;
			case 'textarea':
				model.attributes.value = (isset(model.value)?model.value:'');
				inputTag = 'textarea';
				inputInnerHTML = (isset(model.value)?model.value:'');
				inputType = 'textarea';
			break;
			default:
				inputTag = 'input';
				model.attributes.value = (isset(model.value)?model.value:'');
				model.attributes.type = 'text';
				inputType = 'text';
			break;
		}
		var input = createElement({
			tag: inputTag,
			attributes: model.attributes,
			events: model.events
		});
		input.name = model.attributes.name;
		input.type = model.attributes.type;
		input.value = model.attributes.value;
		if (inputInnerHTML) input.innerHTML = inputInnerHTML;
		children.push(input);
		if (isset(model.label)) children.push(createElement({
			tag: 'span',
			className: 'formControl__label',
			innerText: model.label
		}));
		if (isset(model.badge)) children.push(createElement({
			tag: 'span',
			className: 'formControl__badge',
			innerText: model.badge
		}));
		var element = createElement({
			tag: 'label',
			className: [
							'formControl',
							['__',inputType].join(''),
							(isset(model.active)&&model.active?'__active':''),
							(isset(model.hasError)&&model.hasError?'__hasError':''),
							(isset(model.hasSuccess)&&model.hasSuccess?'__hasSuccess':''),
							(isset(model.box)&&model.box?'__box':''),
							(isset(model.size)&&model.size?['__',model.size].join(''):''),
						].join(' '),
			children: children
		});
		formControl.initControl(element);
		return element;
	},
	initControl: function(element) {
		let input = element.querySelector('input, select, textarea');
		if (input) {
			input.formControl = {
				hasError: formControl.hasError,
				hasSuccess: formControl.hasSuccess,
				toggleActive: formControl.toggleActive,
				labelUpdate: formControl.labelUpdate,
				badgeUpdate: formControl.badgeUpdate,
				unsetActive: formControl.unsetActive,
				setActive: formControl.setActive,
				unsetAvailable: formControl.unsetAvailable,
				setAvailable: formControl.setAvailable,
				badgeHelperUpdate: formControl.badgeHelperUpdate,
				container: element,
				input: input,
				badge: element.querySelector('.formControl__badge'),
				label: element.querySelector('.formControl__label')
			};
			if (['checkbox','radio'].indexOf(input.type)!==-1) {
				input.addEventListener('change', input.formControl.toggleActive);
			}
		}
	},
	init: function() {
		let controls = document.querySelectorAll('.formControl');
		for (let i=(controls.length - 1);i>=0;i--) {
			formControl.initControl(controls[i]);
		}
	}
};
formControl.init();