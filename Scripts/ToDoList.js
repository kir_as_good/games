function ToDoList_(model) {
	var self = this;

	this.name = model.name;

	this.elements = {};
	this.tasksList = [];
	this.render = {
		container: function() {
			removeChildren(self.elements.parent);
			self.elements.container = createElement({
				className: 'ToDoList'
			});
			self.elements.header = createElement({
				tag: 'header',
				className: 'ToDoList__header',
				children: self.render.header()
			});
			self.elements.container.appendChild(self.elements.header);
			self.elements.sidebar = createElement({
				tag: 'aside',
				className: 'ToDoList__sidebar',
				hidden: true,
				children: self.render.sidebar(),
				model: {
					show: function() {
						self.elements.sidebar.hidden = false;
						self.elements.container.classList.add('__showSidebar');
					},
					hide: function() {
						self.elements.sidebar.hidden = true;
						self.elements.container.classList.remove('__showSidebar');
					},
					toggle: function() {
						if (self.elements.sidebar.hidden) {
							self.elements.sidebar.show();
						} else {
							self.elements.sidebar.hide();
						}
					}
				}
			});
			self.elements.container.appendChild(self.elements.sidebar);
			self.elements.body = createElement({
				className: 'ToDoList__body',
				children: self.render.body()
			});
			self.elements.container.appendChild(self.elements.body);
			self.elements.footer = createElement({
				tag: 'footer',
				className: 'ToDoList__footer',
				children: self.render.footer()
			});
			self.elements.container.appendChild(self.elements.footer);

			self.elements.parent.appendChild(self.elements.container);
		},
		header: function() {
			return [
				createElement({
					tag: 'button',
					innerHTML: '<i class="i __i-th-menu"></i>',
					events: {
						click: function() {
							self.elements.sidebar.toggle();
						}
					}
				}),
				createElement({
					tag: 'input',
					attributes: {
						type: 'text',
						placeholder: 'ToDoList'
					},
					events: {
						input: function() {
							console.log(this.value);
						}
					}
				})
			];
		},
		sidebar: function() {
			return [
				createElement({
					tag: 'ul',
					children: [
						createElement({
							tag: 'li',
							className: '__active',
							children: [
								createElement({
									tag: 'button',
									innerHTML: ['<i class="i __i-grid"></i> ',LL.get('Tasks list')].join('')
								}),
								createElement({
									tag: 'ul',
									children: [
										createElement({
											tag: 'li',
											children: [
												createElement({
													tag: 'button',
													innerText: LL.get('Opened')
												})
											]
										}),
										createElement({
											tag: 'li',
											children: [
												createElement({
													tag: 'button',
													innerText: LL.get('Closed')
												})
											]
										})
									]
								})
							]
						}),
						createElement({
							tag: 'li',
							children: [
								createElement({
									tag: 'button',
									innerHTML: ['<i class="i __i-hash"></i> ',LL.get('Add new Task')].join(''),
									events: {
										click: function() {
											self.render.task(null);
										}
									}
								})
							]
						}),
						createElement({
							tag: 'li',
							children: [
								createElement({
									tag: 'button',
									innerHTML: ['<i class="i __i-warning"></i> ',LL.get('Clear all Tasks')].join('')
								})
							]
						}),
					]
				})
			];
		},
		body: function() {
			self.elements.bodyInner = createElement({className:'ToDoList__body__inner'});
			return [];
		},
		footer: function() {
			self.elements.counters = {};
			self.elements.counters.tasks = createElement({tag:'span',className:'ToDoList__counters__tasks'});
			self.elements.counters.opened = createElement({tag:'span',className:'ToDoList__counters__opened'});
			self.elements.counters.closed = createElement({tag:'span',className:'ToDoList__counters__closed'});
			return [self.elements.counters.tasks,self.elements.counters.closed,self.elements.counters.opened];
		},
		task: function(model) {
			removeChildren(self.elements.body);
			var currentModel = (model?Object.assign({},model):{
				title: '',
				date: '',
				time: '',
				desc: '',
				priority: 0,
				badges: [],
				finished: false
			});
			self.elements.body.appendChild(createElement({
				className: 'ToDoList__task',
				children: [
					formControl.renderControl({
						type: 'text',
						name: 'title',
						label: LL.get('Title'),
						size: 'md',
						value: currentModel.title,
						attributes: {
							placeholder: LL.get('Enter Task Title')
						},
						events: {
							input: function() {
								currentModel.title = this.value;
							}
						}
					}),
					formControl.renderControl({
						type: 'date',
						name: 'date',
						label: LL.get('Date'),
						value: currentModel.date,
						events: {
							input: function() {
								currentModel.date = this.value;
							}
						}
					}),
					formControl.renderControl({
						type: 'time',
						name: 'time',
						label: LL.get('Time'),
						value: currentModel.time,
						events: {
							input: function() {
								currentModel.time = this.value;
							}
						}
					}),
					formControl.renderControl({
						type: 'textarea',
						name: 'desc',
						label: LL.get('Description'),
						value: currentModel.desc,
						events: {
							input: function() {
								currentModel.desc = this.value;
							}
						}
					}),
					formControl.renderControl({
						type: 'radio',
						name: 'priority',
						label: LL.get('Low'),
						value: 0,
						active: (currentModel.priority===0?true:false),
						events: {
							change: function() {
								currentModel.priority = 0;
							}
						}
					}),
					formControl.renderControl({
						type: 'radio',
						name: 'priority',
						label: LL.get('Middle'),
						value: 1,
						active: (currentModel.priority===1?true:false),
						events: {
							change: function() {
								currentModel.priority = 1;
							}
						}
					}),
					formControl.renderControl({
						type: 'radio',
						name: 'priority',
						label: LL.get('Hight'),
						value: 2,
						active: (currentModel.priority===2?true:false),
						events: {
							change: function() {
								currentModel.priority = 2;
							}
						}
					}),
					formControl.renderControl({
						type: 'radio',
						name: 'priority',
						label: LL.get('Highest'),
						value: 3,
						active: (currentModel.priority===3?true:false),
						events: {
							change: function() {
								currentModel.priority = 3;
							}
						}
					}),
					formControl.renderControl({
						type: 'checkbox',
						name: 'badges',
						label: LL.get('A'),
						value: 'a',
						active: (currentModel.badges.indexOf('a')===-1?false:true),
						events: {
							change: function() {
								if (this.checked) {
									if (currentModel.badges.indexOf('a')===-1) currentModel.badges.push('a');
								} else {
									var i = currentModel.badges.indexOf('a');
									if (i!=-1) currentModel.badges.splice(i,1);
								}
							}
						}
					}),
					formControl.renderControl({
						type: 'checkbox',
						name: 'badges',
						label: LL.get('B'),
						value: 'b',
						active: (currentModel.badges.indexOf('b')===-1?false:true),
						events: {
							change: function() {
								if (this.checked) {
									if (currentModel.badges.indexOf('b')===-1) currentModel.badges.push('b');
								} else {
									var i = currentModel.badges.indexOf('b');
									if (i!=-1) currentModel.badges.splice(i,1);
								}
							}
						}
					}),
					formControl.renderControl({
						type: 'checkbox',
						name: 'badges',
						label: LL.get('C'),
						value: 'c',
						active: (currentModel.badges.indexOf('c')===-1?false:true),
						events: {
							change: function() {
								if (this.checked) {
									if (currentModel.badges.indexOf('c')==-1) currentModel.badges.push('c');
								} else {
									var i = currentModel.badges.indexOf('c');
									if (i!=-1) currentModel.badges.splice(i,1);
								}
							}
						}
					}),
					createElement({
						tag: 'button',
						className: 'btn __goldGradient __block',
						innerText: LL.get('Save'),
						events: {
							click: function() {
								if (currentModel.title.length>0) {
									// self.tasksList[] = currentModel;
								}
							}
						}
					})
				]
			}));
		}
	};

	this.load = function() {
		let test = window.localStorage.getItem(['ToDoList',self.name].join('_'));
		if (test) self.tasksList = JSON.parse(test);
	};
	this.save = function() {
		window.localStorage.setItem(['ToDoList', self.name].join('_'), JSON.stringify(self.tasksList));
	};

	this.init = function() {
		self.elements.parent = document.getElementById(self.name);
		self.load();
		self.render.container();
	};
	this.init();
}