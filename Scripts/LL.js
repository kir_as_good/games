window.LL = {
	current: 'uk',
	langList: {
		uk: {titleRU:'Украинский',titleUK:'Українська',titleEn:'Ukraine'},
		ru: {titleRU:'Русский',titleUK:'Російська',titleEn:'Russian'},
		en: {titleRU:'Английский',titleUK:'Англійська',titleEn:'English'}
	},
	changeLang: function(l) {
		if (isset(LL.langList[l])) {
			LL.current = l;
		}
	},
	get: function(str) {
		return str;
	},
	getMethering: function(str, v) {
		var lastDigits 	= v % 100,
			metering 	= "";
		lastDigits = (lastDigits >= 20) ? lastDigits % 10 : lastDigits;
		if (lastDigits === 0 ||
			lastDigits >= 5 && lastDigits <= 20) {
			switch (str) {
				case 'adults':
					metering = LL.get('дорослих');
				break;
			}
		} else {
			if (lastDigits === 1) {
				switch (str) {
					case 'adults':
						metering = LL.get('дорослий');
					break;
				}
			} else {
				switch (str) {
					case 'adults':
						metering = LL.get('дорослих');
					break;
				}
			}
		}
		return metering;
	}
};