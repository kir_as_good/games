window.layout = {
	elements: {},
	phonesModal: function() {
		if (!isset(layout.elements.phonesModal)) {
			layout.elements.phonesModal = new Modal({
				elementID: 'CallBackModal'
			});
		}
		layout.elements.phonesModal.show();
	},
	menuSidebar: function() {
		if (!isset(layout.elements.menuSidebar)) {
			layout.elements.menuSidebar = new Sidebar({
				elementID: 'headerMenu'
			});
		}
		layout.elements.menuSidebar.show();
	},
	initAboutModal: function() {
		if (!isset(layout.elements.AboutModal)) {
			layout.elements.AboutModal = new Modal({
				elementID: 'AboutModal'
			});
		}
		layout.elements.AboutModal.show();
	},
	init: function() {
		layout.elements.header__menuButton = document.getElementById('header__menuButton');
		layout.elements.header__menuButton.addEventListener('click', layout.menuSidebar);
		layout.elements.header__phoneButton = document.getElementById('header__phoneButton');
		layout.elements.header__phoneButton.addEventListener('click', layout.phonesModal);
	}
};

onload__add('layout.init', layout.init);