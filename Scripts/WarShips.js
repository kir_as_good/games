function WarShips_() {
	let self = this;
	this.container = null;
	this.model = {
		field: {
			rows: [1,2,3,4,5,6,7,8,9,10],
			cols: ['a','b','c','d','e','f','g','h','i','j']
		},
		ships: [1,1,1,1,2,2,2,3,3,4],
		currentStep: 0,
		userMatrix: {},
		pcMatrix: {}
	};
	this.render = {
		matrix: function() {
			let col, row,
			matrix = {};
			for (row=0;row<self.model.field.rows.length;row++) {
				matrix[self.model.field.rows[row]] = {};
				for (col=0;col<self.model.field.cols.length;col++) {
					matrix[self.model.field.rows[row]][self.model.field.cols[col]] = {
						value: false,
						row: self.model.field.rows[row],
						col: self.model.field.cols[col],
						matrix: matrix
					};
				}
			}
			return matrix;
		},
		userField: function() {
			let row, col, nodes = [],
				fragment = document.createDocumentFragment();
			for (row in self.model.userMatrix) {
				for (col in self.model.userMatrix[row]) {
					fragment.appendChild(createElement({
						tag: 'button',
						model: self.model.userMatrix[row][col],
						events: {
							click: self.events.setShipPoint
						}
					}));
				}
			}
			return fragment;
		},
	};
	this.events = {
		setShipPoint: function(e) {
			if (this.value==='true' || this.value===true) {
				this.value = false;
				this.matrix[this.row][this.col].value = false;
			} else {
				this.value = true;
				this.matrix[this.row][this.col].value = true;
				let checkIfShipPossible = true;
				if (checkIfShipPossible) {
					this.value = true;
				}
			}
		},
		fire: function(e) {}
	};
	this.setShips = function(matrix) {
	};
	this.Start = function() {
		self.userField = createElement({tag:'div',className:'WarShips__field'});
		self.model.userMatrix = self.render.matrix();
		self.userField.appendChild(self.render.userField());
		self.container.appendChild(self.userField);

		// self.model.pcMatrix = self.setShips(self.render.matrix());
	};
	this.Stop = function() {};
	this.init = function() {
		let mainCountainer = document.getElementById('content__container');
		removeChildren(mainCountainer);
		self.container = createElement({
			tag: 'div',
			className: 'WarShips'
		});
		mainCountainer.appendChild(self.container);
		self.Start();
	};
	this.init();
}