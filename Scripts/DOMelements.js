function createElement(model) {
	let element,
		i,
		objectName,
		style,
		attribute,
		event,
		modelItem;
	if (typeof model=='object') {
		element = document.createElement(isset(model.tag)?model.tag:'div');
		if (isset(model.id)) element.id = model.id;
		if (isset(model.className)) element.className = model.className;
		if (isset(model.innerHTML)) element.innerHTML = model.innerHTML;
		if (isset(model.innerText)) element.innerText = model.innerText;
		if (isset(model.hidden)) element.hidden = model.hidden;
		if (isset(model.model)) {
			for (objectName in model.model) {
				element[objectName] = model.model[objectName];
			}
		}
		if (isset(model.style)) {
			for (style in model.style) {
				element.style[style] = model.style[style];
			}
		}
		if (isset(model.attributes)) {
			for (attribute in model.attributes) {
				element.setAttribute(attribute, model.attributes[attribute]);
			}
		}
		if (isset(model.model)) {
			for (modelItem in model.model) {
				element[modelItem] = model.model[modelItem];
			}
		}
		if (isset(model.events)) {
			for (event in model.events) {
				element.addEventListener(event, model.events[event]);
			}
		}
		if (isset(model.appends)) model.children = (isset(model.children)?model.children.concat(model.appends):model.appends);
		if (isset(model.children)) {
			for (i=0;i<model.children.length;i++) {
				element.appendChild(model.children[i]);
			}
		}
		if (isset(model.prepends)) {
			for (i=(model.prepends.length - 1);i>=0;i--) {
				if (isset(element.firstChild)&&element.firstChild) {
					element.insertBefore(model.prepends[i], element.firstChild);
				} else {
					element.appendChild(model.childs[i]);
				}
			}
		}
	} else {
		element = document.createElement(model);
	}
	return element;
}

function removeChildren(element) {
	while (element.firstChild) {
		element.removeChild(element.firstChild);
	}
}

function addCss(fileName) {
	let body = document.body,
		link = document.createElement('link');
	link.type = 'text/css';
	link.rel = 'stylesheet';
	link.href = fileName;
	body.appendChild(link);
}
function addScript(fileName) {
	let body = document.body,
		link = document.createElement('script');
	link.type = 'text/javascript';
	link.src = fileName;
	body.appendChild(link);
}

function randomId() {
	let f = function(){
			return String.fromCharCode(65 + Math.floor(Math.random() * 26)) + Date.now();
		},
		a = f();
	while (document.getElementById(a)) {
		a = f();
	}
	return a;
}