function inputMask(model) {
	let self = this;
	this.model = model;
	this.getPattern = function() {
		let pattern = (isset(self.model.pattern)?self.model.pattern:self.model.input.getAttribute('data-pattern')),
			new_pattern = [],
			i;
		pattern.split('');
		for (i=0;i<pattern.length;i++) {
			switch (pattern[i]) {
				case '0':
					new_pattern.push({f: self.symbolDigit, s:pattern[i]});
				break;
				case 'Z':
					new_pattern.push({f: self.symbolLetter, s:pattern[i]});
				break;
				default:
					new_pattern.push({f: self.symbolAny, s:pattern[i]});
				break;
			}
		}
		return new_pattern;
	};
	this.codes = [49,50,51,52,53,54,55,56,57,48,189,187,192,81,87,69,82,84,89,85,73,79,80,219,221,220,65,83,68,70,71,72,74,75,76,186,222,90,88,67,86,66,78,77,188,190,191,103,104,105,100,101,102,97,98,99,110,96,111,106,109];
	this.onInput = function(e) {
		let char = self.model.input.value.substr(-1),
			code = e.which || char.charCodeAt(0);
		if (char.length===1&&self.codes.indexOf(code)!==-1) {
			e.preventDefault();
			let new_value = [],
				value = self.model.input.value.split(''),
				i, obj, v;
			value.push(char.toString());
			for (i=0;i<self.pattern.length;i++) {
				v = (isset(value[i])?value[i]:null);
				obj = self.pattern[i].f(v, self.pattern[i].s);
				if (obj==null) {
					break;
				} else {
					new_value.push(obj);
				}
			}
			new_value = new_value.join('');
			self.previous_value = new_value;
			self.model.input.value = new_value;
		}
		switch (code) {
			case 8:
			break;
			default:
			break;
		}
		return false;
	};
	this.symbolDigit = function(v, s) {
		if (v==null) return null;
		return (v.toLowerCase().match(/[0-9]/i)?v:null);
	};
	this.symbolLetter = function(v, s) {
		if (v==null) return null;
		return (v.toLowerCase().match(/[a-zа-яіїєэ]/i)?v:null);
	};
	this.symbolAny = function(v, s) {
		return s;
	};
	this.init = function() {
		self.model.input.value = (self.model.input.value?self.model.input.value:'');
		self.previous_value = self.model.input.value;
		self.new_value = self.model.input.value;
		self.pattern = self.getPattern();
		self.model.input.addEventListener('input', self.onInput);
	};
	this.init();
}