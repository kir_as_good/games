function starsRender(v) {
	if (isNaN(v)||v<1) return '';
	let html = [];
	html.push('<span class="stars">');
	html.push(starsRenderInner(v));
	html.push('</span>');
	return html.join('');
}

function starsRenderInner(v) {
	if (isNaN(v)||v<1) return '';
	let html = [],
		i = 5;
	while (v>0) {
		html.push('');
		v--;
		i--;
	}
	while (i>0) {
		html.push('');
		i--;
	}
	return html.join('');
}