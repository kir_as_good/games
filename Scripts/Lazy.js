function Lazy(image) {
	let self = this;
	this.image = image;
	this.image.id = (isset(this.image.id)&&this.image.id.length>0?this.image.id:randomId());
	this.precheck = function() {
		self.observer.observe(self.image);
	};
	this.checkAndSet = function(entries) {
		if (entries[0].intersectionRatio > 0) {
			self.observer.unobserve(entries[0].target);
			self.setSrc();
		}
	};
	this.setSrc = function() {
		self.image.loaded = true;
		self.image.setAttribute('src', self.image.imgSRC);
		self.image.removeAttribute('data-src');
		self.image.classList.add('__loaded');
		self.observer.unobserve(self.image);
	};
	this.init = function() {
		self.image.loaded = false;
		self.image.imgSRC = self.image.getAttribute('data-src')||self.image.getAttribute('src');
		self.image.setAttribute('src','');
		let config = {
			rootMargin: '50px 0px',
			threshold: 0.01
		};
		self.observer = new IntersectionObserver(self.checkAndSet, config);
	};
	this.init();
}
