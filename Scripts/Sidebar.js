function Sidebar(model) {
	let self = this;
	this.elements = {};
	this.model = (isset(model)?model:{});
	this.body = document.getElementById('body');

	this.render = function() {
		self.elements.parent = createElement({
			tag: 'div',
			id: (isset(self.model.id)?self.model.id:randomId()),
			className: ['Sidebar', (isset(self.model.className)?self.model.className:'')].join(' '),
			hidden: true
		});
		let documentFragment = document.createDocumentFragment(),
			box = createElement({
			tag: 'div',
			className: 'Sidebar__darker',
			events: {
				click: self.hide
			}
		});
		documentFragment.appendChild(box);
		let container = createElement({
			tag: 'div',
			className: 'Sidebar__container'
		});
		documentFragment.appendChild(container);
		self.elements.header = createElement({
			tag: 'div',
			className: 'Sidebar__container__header',
			innerHTML: ['<span>',(isset(model.headerContent)?model.headerContent:''),'</span>'].join(''),
			events: {
				click: self.hide
			}
		});
		container.appendChild(self.elements.header);
		self.elements.body = createElement({
			tag: 'div',
			className: 'Sidebar__container__body',
			innerHTML: (isset(model.bodyContent)?model.bodyContent:'')
		});
		container.appendChild(self.elements.body);
		self.elements.footer = createElement({
			tag: 'div',
			className: 'Sidebar__container__footer',
			innerHTML: (isset(model.footerContent)?model.footerContent:'')
		});
		container.appendChild(self.elements.footer);
		self.elements.parent.appendChild(documentFragment);
	};
	this.getRendered = function(id) {
		self.elements.parent = document.getElementById(id);
		self.elements.header = self.elements.parent.getElementsByClassName('Sidebar__container__header')[0];
		self.elements.body = self.elements.parent.getElementsByClassName('Sidebar__container__body')[0];
		self.elements.footer = self.elements.parent.getElementsByClassName('Sidebar__container__footer')[0];
		var darker = self.elements.parent.getElementsByClassName('Sidebar__darker')[0];
		darker.addEventListener('click', self.hide);
		self.elements.header.addEventListener('click', self.hide);
	};
	this.show = function() {
		if (self.elements.parent.hidden) {
			self.elements.parent.hidden = false;
			self.body.style.overflow = 'hidden';
		}
	};
	this.hide = function() {
		if (!self.elements.parent.hidden) {
			self.elements.parent.hidden = true;
			self.body.style.overflow = '';
		}
	};

	this.init = function() {
		if (isset(model.elementID)) {
			self.getRendered(model.elementID);
		} else {
			self.render();
			self.body.appendChild(self.elements.parent);
		}
	};
	this.init();
}