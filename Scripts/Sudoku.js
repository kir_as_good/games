function Sudoku_() {
	let self = this,
		matrix = {};
	this.elements = {
		parent: null,
		header: null,
		container: null,
		footer: null,
		counters: {
			generated: null,
			error: null,
			good: null
		}
	};
	this.counters = {
		generated: 0,
		error: 0,
		good: 0
	};
	this.maxLevel = 7;
	this.level = 1;
	this.generator = {
		possible: [1, 2, 3, 4, 5, 6, 7, 8, 9],
		n: 0,
		max: 500,
		matrix: function() {
			let containError = false;
			self.generator.n++;
			self.counters.generated++;
			self.elements.counters.generated.innerText = self.counters.generated;
			for (let row=1;row<=9;row++) {
				matrix[row] = {};
				if (!self.generator.row(row)) {
					containError = true;
				}
			}
			if (self.generator.n>=self.generator.max||containError) {
				self.generator.n = 0;
				setTimeout(self.generator.matrix,1);
				return;
			}
			self.render();
		},
		row: function(row) {
			for (let col=1;col<=9;col++) {
				if (!self.generator.cell(row, col)) {
					return false;
				}
			}
			return true;
		},
		cell: function(row, col) {
			row = parseInt(row);
			col = parseInt(col);
			let possible = self.generator.possible.slice(0), x, y, z;
			for (x in matrix) {
				x = parseInt(x);
				z = possible.indexOf(matrix[x][col]);
				if (z>-1) {
					possible.splice(z,1);
				}
				if (x===row) {
					for (y in matrix[row]) {
						z = possible.indexOf(matrix[row][y]);
						if (z>-1) {
							possible.splice(z,1);
						}
					}
				}
				if ((row>=1&&row<=3&&x>=1&&x<=3)||(row>=4&&row<=6&&x>=4&&x<=6)||(row>=7&&row<=9&&x>=7&&x<=9)) {
					for (y in matrix[x]) {
						if ((col>=1&&col<=3&&y>=1&&y<=3)||(col>=4&&col<=6&&y>=4&&y<=6)||(col>=7&&col<=9&&y>=7&&y<=9)) {
							z = possible.indexOf(matrix[x][y]);
							if (z>-1) {
								possible.splice(z,1);
							}
						}
					}
				}
			}
			if (possible.length) {
				matrix[row][col] = possible[randomInteger(0, (possible.length - 1))];
				return true;
			} else {
				return false;
			}
		}
	};
	this.inputs = [];
	this.randomMatrix = function(count) {
		let randoms = [],
			array = [],
			i, ii, rand;
		for (i=9;i>0;i--) {
			for (ii=9;ii>0;ii--) {
				array.push([i,ii].join('-'));
			}
		}
		while (count) {
			count--;
			rand = randomInteger(0, (array.length - 1));
			randoms.push(array[rand]);
			array.splice(rand, 1);
		}
		return randoms;
	};
	this.render = function() {
		let row, col,
			filledCount = 0,
			requireFilled = (90 - (self.level * 9) - 9),
			randoms = self.randomMatrix(requireFilled),
			documentFragment = document.createDocumentFragment();
		for (row in matrix) {
			for (col in matrix[row]) {
				let done = randoms.indexOf([row,col].join('-'))!==-1,
					value = (done?matrix[row][col]:''),
					label = createElement({tag:'label'}),
					input = createElement({
					tag: 'input',
					attributes: {
						name: ['Sudoku_Cell[',row,'][',col,']'].join(''),
						value: value,
						type: 'number',
						min: 1,
						max: 9,
						realValue: matrix[row][col],
						row: row,
						col: col
					},
					events: {
						input: self.cell.input
					}
				});
				if (done) {
					input.disabled = true;
					requireFilled--;
					filledCount++;
				}
				label.appendChild(input);
				documentFragment.appendChild(label);
				self.inputs.push({row:row, col:col, input:input, done:done});
			}
		}
		self.elements.counters.filled.innerText = filledCount;
		removeChildren(self.elements.container);
		self.elements.container.appendChild(documentFragment);
	};
	this.cell = {
		input: function() {
			let i,
				row 		= parseInt(this.getAttribute('row')),
				col 		= parseInt(this.getAttribute('col')),
				value 		= parseInt(this.value),
				realValue 	= parseInt(this.getAttribute('realValue'));
			value = (value<10&&value>0?value:0);
			if (value!==realValue) {
				self.counters.error++;
				self.elements.counters.errors.innerText = self.counters.error;
				this.classList.add('hasError');
			} else {
				if (!isset(self.setted)) self.setted = {};
				if (!isset(self.setted[row])) self.setted[row] = {};
				if (!isset(self.setted[row][col])) self.setted[row][col] = '';
				self.setted[row][col] = value;
				self.counters.good++;
				self.elements.counters.goods.innerText = self.counters.good;
				this.classList.remove('hasError');
				this.removeEventListener('input', self.cell.input);
				let done = true;
				for (i=(self.inputs.length - 1);i>=0;i--) {
					if (parseInt(self.inputs[i].row)===row&&parseInt(self.inputs[i].col)===col) {
						self.inputs[i].done = true;
					}
					if (!self.inputs[i].done) done = false;
				}
				if (done) {
					self.Stop();
				}
			}
		}
	};
	this.Stop = function() {
		self.elements.header.hidden = false;
		self.elements.gameResult.innerText = LL.get('Win!');
	};
	this.init = function() {
		let content = document.getElementById('content__container');
		self.elements.parent = createElement({
			className: 'Sudoku'
		});
		self.elements.header = createElement({
			className: 'Sudoku__header'
		});
		self.elements.gameResult = createElement();
		self.elements.header.appendChild(self.elements.gameResult);
		let formControlRow = createElement({
				className: 'formControlRow'
			}),
			choices = {},
			i=0;
		while(i<self.maxLevel) {
			i++;
			choices[i] = i;
		}
		formControlRow.appendChild(formControl.renderControl({
			type: 'select',
			value: self.level,
			name: 'level',
			choices: choices,
			events: {
				change: function() {
					self.level = parseInt(this.value);
				}
			}
		}));
		formControlRow.appendChild(createElement({
			tag: 'button',
			innerText: LL.get('New Game'),
			className: 'btn __goldGradient',
			events: {
				click: function() {
					self.elements.header.hidden = true;
					self.generator.n = 0;
					self.counters.generated = 0;
					matrix = {};
					self.generator.matrix();
				}
			}
		}));
		self.elements.header.appendChild(formControlRow);
		self.elements.parent.appendChild(self.elements.header);
		self.elements.container = createElement({
			className: 'Sudoku__container'
		});
		self.elements.parent.appendChild(self.elements.container);
		self.elements.footer = createElement({
			className: 'Sudoku__footer'
		});
		self.elements.counters.errors = createElement({
			className: 'Sudoku__errors',
			innerText: 0
		});
		self.elements.footer.appendChild(self.elements.counters.errors);
		self.elements.counters.goods = createElement({
			className: 'Sudoku__goods',
			innerText: 0
		});
		self.elements.footer.appendChild(self.elements.counters.goods);
		self.elements.counters.generated = createElement({
			className: 'Sudoku__generated',
			innerText: 0
		});
		self.elements.footer.appendChild(self.elements.counters.generated);
		self.elements.counters.filled = createElement({
			className: 'Sudoku__filled',
			innerText: 0
		});
		self.elements.footer.appendChild(self.elements.counters.filled);
		self.elements.parent.appendChild(self.elements.footer);
		content.appendChild(self.elements.parent);
	};
	this.init();
}
