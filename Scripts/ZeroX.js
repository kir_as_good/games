function ZeroX_() {
	let self = this;
	this.fields = {};
	this.container = null;
	this.render = {
		container: function() {
			self.container = createElement({tag: 'div', id: 'ZeroX'});
			document.getElementById('content__container').appendChild(self.container);
			self.render.fields();
		},
		fields: function() {
			for (let x in self.fields) {
				for (let y in self.fields[x]) {
					self.render.field({x:x,y:y});
				}
			}
		},
		field: function(model) {
			let field = createElement({tag: 'div', id: 'ZeroX__'+model.x+'_'+model.y});
			field.x = model.x;
			field.y = model.y;
			field.set = function(m){
				this.innerHTML = (m?'X':'0');
				self.fields[this.x][this.y] = (m?1:2);
				if (!self.isFinished(1)) {
					if (m) self.aiTurn();
				} else {
					self.Stop();
				}
				this.removeEventListener('click', this.click);
			};
			field.click = function(e){this.set(true);};
			field.addEventListener('click', field.click);
			self.container.appendChild(field);
		}
	};
	this.getPossible = function() {
		let possible = [];
		for (let x in self.fields) {
			for (let y in self.fields[x]) {
				if (self.fields[x][y]===0) {
					possible.push({x:x,y:y});
				}
			}
		}
		return possible;
	};
	this.aiTurn = function() {
		let field = null,
			possible = self.getPossible(),
			required = self.getRequired(possible),
			bests = self.getBest(possible);
		if (possible.length) {
			if (bests.length) {
				field = bests[randomInteger(0, (bests.length - 1))];
			} else if (required.length) {
				field = required[randomInteger(0, (required.length - 1))];
			} else {
				field = possible[randomInteger(0, (possible.length - 1))];
			}
			document.getElementById('ZeroX__'+field.x+'_'+field.y).set(false);
			if (self.isFinished(2)) self.Stop();
		} else {
			self.Stop();
		}
	};
	this.getRequired = function(possible) {
		let required = [];
		for (let i=(possible.length - 1);i>=0;i--) {
			self.fields[possible[i].x][possible[i].y]=1;
			let check = self.getWinner(1);
			if (check) required.push(possible[i]);
			self.fields[possible[i].x][possible[i].y]=0;
		}
		return required;
	};
	this.getBest = function(possible) {
		let bests = [];
		for (let i=(possible.length - 1);i>=0;i--) {
			self.fields[possible[i].x][possible[i].y]=2;
			let check = self.getWinner(2);
			if (check) bests.push(possible[i]);
			self.fields[possible[i].x][possible[i].y]=0;
		}
		return bests;
	};
	this.isFinished = function(i) {
		return self.getPossible().length===0||self.getWinner(i);
	};
	this.getWinner = function(i) {
		return self.checkRows(i)!==false||self.checkCollumns(i)!==false||self.checkDiagonals(i)!==false;
	};
	this.checkRows = function(i) {
		if (self.fields[0][0]===i&&self.fields[0][1]===i&&self.fields[0][2]===i) {
			return 0;
		} else if (self.fields[1][0]===i&&self.fields[1][1]===i&&self.fields[1][2]===i) {
			return 1;
		} else if (self.fields[2][0]===i&&self.fields[2][1]===i&&self.fields[2][2]===i) {
			return 2;
		} else {
			return false;
		}
	};
	this.checkCollumns = function(i) {
		if (self.fields[0][0]===i&&self.fields[1][0]===i&&self.fields[2][0]===i) {
			return 0;
		} else if (self.fields[0][1]===i&&self.fields[1][1]===i&&self.fields[2][1]===i) {
			return 1;
		} else if (self.fields[0][2]===i&&self.fields[1][2]===i&&self.fields[2][2]===i) {
			return 2;
		} else {
			return false;
		}
	};
	this.checkDiagonals = function(i) {
		if (self.fields[0][0]===i&&self.fields[1][1]===i&&self.fields[2][2]===i) {
			return 0;
		} else if (self.fields[0][2]===i&&self.fields[1][1]===i&&self.fields[2][0]===i) {
			return 1;
		} else {
			return false;
		}
	};
	this.generate = function() {
		let model = {};
		for (let i=0;i<3;i++) {
			model[i] = {};
			for (let ii=0;ii<3;ii++) {
				model[i][ii] = 0;
			}
		}
		return model;
	};
	this.Save = function() {
		let currentGame = {};
		localStorage.setItem('ZeroX', JSON.stringify(currentGame));
	};
	this.Load = function() {
		let currentGame = localStorage.getItem('ZeroX');
		return (currentGame?JSON.parse(currentGame):self.generate());
	};
	this.Win = function() {
		let msg = createElement({tag: 'span', innerHTML: 'You Win', style: {background:'var(--c-green)'}});
		self.container.appendChild(msg);
	};
	this.Loose = function() {
		let msg = createElement({tag: 'span', innerHTML: 'You Loose', style: {background:'var(--c-red)'}});
		self.container.appendChild(msg);
	};
	this.DeadHead = function() {
		let msg = createElement({tag: 'span', innerHTML: 'Dead Head', style: {background:'var(--c-azure)'}});
		self.container.appendChild(msg);
	};
	this.Start = function() {
		self.fields = self.Load();
		self.render.container();
	};
	this.Stop = function() {
		let x, y, el;
		for (x in self.fields) {
			for (y in self.fields[x]) {
				el = document.getElementById('ZeroX__'+x+'_'+y);
				el.removeEventListener('click', el.click);
			}
		}
		if (self.getWinner(1)) {
			self.Win();
		} else if (self.getWinner(2)) {
			self.Loose();
		} else {
			self.DeadHead();
		}
	};
	this.Start();
}
