function randomInteger(min, max) {
	let rand = min + Math.random() * (max + 1 - min);
	rand = Math.floor(rand);
	return rand;
}

function isset(i) {
	return (typeof i!='undefined');
}

function getCurrencySymbol(s) {
	let r;
	switch (s.toLowerCase()) {
		case 'usd':
			r = '$';
		break;
		case 'eur':
			r = '€';
		break;
		case 'uah':
			r = '₴';
		break;
		default:
			r = s;
		break;
	}
	return r;
}

function intHumanize(n) {
	let i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(2))),
		j = (j = i.length) > 3 ? j % 3 : 0;
	return (j ? i.substr(0, j) + " " : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + " ");
}

Date.prototype.addDays = function(days) {
	let date = new Date(this.valueOf());
	date.setDate(date.getDate() + days);
	return date;
};

Date.prototype.clone = function () {
	return new Date(this.getTime());
};

Date.prototype.diff = function(that, points) {
	let diffTime 	= Math.abs(this.getTime() - that.getTime()),
		diff 		= 0;
	switch (points) {
		case 'days':
			diff = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
		break;
		case 'hours':
			diff = Math.ceil(diffTime / (1000 * 60 * 60));
		break;
		case 'minutes':
			diff = Math.ceil(diffTime / (1000 * 60));
		break;
		case 'seconds':
			diff = Math.ceil(diffTime / 1000);
		break;
		default:
			diff = diffTime;
		break;
	}
	return diff;
};

Array.prototype.max = function() {
	return Math.max.apply(null, this);
};
Array.prototype.min = function() {
	return Math.min.apply(null, this);
};
Array.prototype.clone = function() {
	return this.slice(0);
};
Array.prototype.diff = function(a) {
	return this.filter(function(i){return a.indexOf(i) < 0;});
};