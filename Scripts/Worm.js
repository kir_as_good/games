function Worm_() {
	let self = this;
	this.width = 50;
	this.height = 50;
	this.fruit = 6;
	this.worm = {direction:'right',points:[{x:0,y:0}]};
	this.fields = {};
	this.container = null;
	this.interval = null;
	this.speed = 50000;
	this.controls = {};
	this.nav = {
		up: function() {
			self.worm.direction = 'up';
			self.controls.left.hidden = false;
			self.controls.right.hidden = false;
			self.controls.up.hidden = true;
			self.controls.down.hidden = true;
		},
		down: function() {
			self.worm.direction = 'down';
			self.controls.left.hidden = false;
			self.controls.right.hidden = false;
			self.controls.up.hidden = true;
			self.controls.down.hidden = true;
		},
		left: function() {
			self.worm.direction = 'left';
			self.controls.left.hidden = true;
			self.controls.right.hidden = true;
			self.controls.up.hidden = false;
			self.controls.down.hidden = false;
		},
		right: function() {
			self.worm.direction = 'right';
			self.controls.left.hidden = true;
			self.controls.right.hidden = true;
			self.controls.up.hidden = false;
			self.controls.down.hidden = false;
		}
	};
	this.render = {
		container: function() {
			self.container = createElement('div');
			self.container.id = 'Worm';
			self.render.controls();
			self.render.field();
			self.render.worm();
			document.getElementById('content__container').appendChild(self.container);
		},
		controls: function() {
			self.controls.up 	= createElement('button');
			self.controls.up.addEventListener('click', self.nav.up);
			self.controls.up.hidden = true;
			self.container.appendChild(self.controls.up);
			self.controls.down 	= createElement('button');
			self.controls.down.addEventListener('click', self.nav.down);
			self.controls.down.hidden = false;
			self.container.appendChild(self.controls.down);
			self.controls.left 	= createElement('button');
			self.controls.left.addEventListener('click', self.nav.left);
			self.controls.left.hidden = true;
			self.container.appendChild(self.controls.left);
			self.controls.right = createElement('button');
			self.controls.right.addEventListener('click', self.nav.right);
			self.controls.right.hidden = true;
			self.container.appendChild(self.controls.right);
		},
		field: function() {
			let documentFragment = document.createDocumentFragment();
			for (let x=0;x<self.width;x++) {
				self.fields[x] = {};
				for (let y=0;y<self.height;y++) {
					self.fields[x][y] = createElement('div');
					self.fields[x][y].style.width = (100/self.width)+'%';
					self.fields[x][y].fruit = false;
					self.fields[x][y].worm = false;
					self.fields[x][y].wormOn = function() {
						this.worm = true;
						this.classList.add('__worm');
						this.classList.remove('__fruit');
					};
					self.fields[x][y].fruitOn = function() {
						if (!this.worm) {
							this.fruit = true;
							this.classList.add('__fruit');
							this.classList.remove('__worm');
						}
					};
					self.fields[x][y].Off = function() {
						this.fruit = false;
						this.worm = false;
						this.classList.remove('__fruit');
						this.classList.remove('__worm');
					};
					documentFragment.appendChild(self.fields[x][y]);
				}
			}
			self.container.appendChild(documentFragment);
		},
		worm: function() {
			self.worm.renderPoints = function() {
				for (let x=0;x<self.width;x++) {
					for (var y=0;y<self.height;y++) {
						if (self.fields[x][y].worm) self.fields[x][y].Off();
					}
				}
				for (let i=0;i<self.worm.points.length;i++) {
					self.fields[self.worm.points[i].x][self.worm.points[i].y].wormOn();
				}
			};
			self.worm.nextPoint = function() {
				let firstPoint = self.worm.points[0],
					model;
				switch (self.worm.direction) {
					case 'left':
						if ((firstPoint.y - 1)<0) return undefined;
						model = {x:firstPoint.x,y:(firstPoint.y - 1)};
					break;
					case 'right':
						if ((firstPoint.y + 1)===self.width) return undefined;
						model = {x:firstPoint.x,y:(firstPoint.y + 1)};
					break;
					case 'up':
						if ((firstPoint.x - 1)<0) return undefined;
						model = {y:firstPoint.y,x:(firstPoint.x - 1)};
					break;
					case 'down':
						if ((firstPoint.x + 1)===self.height) return undefined;
						model = {y:firstPoint.y,x:(firstPoint.x + 1)};
					break;
				}
				return model;
			};
			self.worm.step = function() {
				let nextPoint = self.worm.nextPoint();
				if (nextPoint===undefined) {
					self.Stop();
				} else {
					if (self.fields[nextPoint.x][nextPoint.y].fruit) {
						self.fields[nextPoint.x][nextPoint.y].Off();
					} else if (self.fields[nextPoint.x][nextPoint.y].worm) {
						self.Stop();
					} else {
						self.worm.points.splice(-1, 1);
					}
					self.worm.points.unshift(nextPoint);
				}
				self.worm.renderPoints();
			};
			self.worm.renderPoints();
		},
		fruit: function() {
			if (!randomInteger(0, self.fruit)) {
				let x = randomInteger(0, (self.width - 1)),
					y = randomInteger(0, (self.height - 1));
				self.fields[x][y].fruitOn();
			}
		}
	};
	this.Iterate = function() {
		self.render.fruit();
		self.worm.step();
		if (self.speed>1000) self.speed--;
		self.interval = setTimeout(self.Iterate, (self.speed/1000 * 5));
	};
	this.Stop = function() {
		clearTimeout(self.interval);
		self.Iterate = function() {};
		let msg = createElement('span');
		msg.innerHTML = 'Your score: '+self.worm.points.length;
		self.container.appendChild(msg);
	};
	this.Start = function() {
		self.render.container();
		self.interval = setTimeout(self.Iterate, (self.speed/1000 * 5));
		document.addEventListener('keydown', function(e){
			switch (e.keyCode) {
				case 37:
					if (self.worm.direction==='right') return;
					self.nav.left();
				break;
				case 38:
					if (self.worm.direction==='down') return;
					self.nav.up();
				break;
				case 39:
					if (self.worm.direction==='left') return;
					self.nav.right();
				break;
				case 40:
					if (self.worm.direction==='up') return;
					self.nav.down();
				break;
			}
		});
	};
	this.Start();
}
