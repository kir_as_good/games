function Modal(model) {
	let self = this;
	this.elements = {};
	this.model = (isset(model)?model:{});
	this.body = document.getElementById('body');

	this.render = function() {
		self.elements.parent = createElement({
			tag: 'div',
			id: (isset(self.model.id)?self.model.id:randomId()),
			className: ['Modal', (isset(self.model.className)?self.model.className:''),(isset(self.model.size)?'__'+self.model.size:'')].join(' '),
			hidden: true
		});
		let documentFragment = document.createDocumentFragment(),
			box = createElement({
			tag: 'div',
			className: 'Modal__darker',
			events: {
				click: self.hide
			}
		});
		documentFragment.appendChild(box);
		self.elements.container = createElement({
			tag: 'div',
			className: 'Modal__container'
		});
		documentFragment.appendChild(self.elements.container);
		let closer = createElement({
			tag: 'div',
			className: 'Modal__container__closer',
			events: {
				click: self.hide
			}
		});
		self.elements.container.appendChild(closer);
		self.elements.content = createElement({
			tag: 'div',
			className: 'Modal__container__content',
			innerHTML: (isset(model.innerHTML)?model.innerHTML:'')
		});
		self.elements.container.appendChild(self.elements.content);
		self.elements.parent.appendChild(documentFragment);
	};
	this.getRendered = function(id) {
		self.elements.parent = document.getElementById(id);
		self.elements.container = self.elements.parent.getElementsByClassName('Modal__container')[0];
		self.elements.content = self.elements.parent.getElementsByClassName('Modal__container__content')[0];
		let darker = self.elements.parent.getElementsByClassName('Modal__darker')[0],
			closer = self.elements.parent.getElementsByClassName('Modal__container__closer')[0];
		darker.addEventListener('click', self.hide);
		closer.addEventListener('click', self.hide);
	};
	this.show = function() {
		if (self.elements.parent.hidden) {
			self.elements.parent.hidden = false;
			self.body.style.overflow = 'hidden';
		}
	};
	this.hide = function() {
		if (!self.elements.parent.hidden) {
			self.elements.parent.hidden = true;
			self.body.style.overflow = '';
		}
	};

	this.init = function() {
		if (isset(model.elementID)) {
			self.getRendered(model.elementID);
		} else {
			self.render();
			self.body.appendChild(self.elements.parent);
		}
	};
	this.init();
}