var doCache = true;

var CACHE_NAME = 'games-pwa-cache-v2';

self.addEventListener('activate', function(event) {
  var cacheWhitelist = [CACHE_NAME];
  event.waitUntil(
    caches.keys().then(function(keyList) {
        Promise.all(keyList.map(function(key) {
          if (!cacheWhitelist.includes(key)) {
            console.log('Deleting cache: ' + key);
            return caches.delete(key);
          }
        }));
      }
    )
  );
});

self.addEventListener('install', function(event) {
  if (doCache) {
    event.waitUntil(
      caches.open(CACHE_NAME).then(function(cache) {
        fetch('./manifest.json').then(function(response){
          response.json();
        }).then(function(assets) {
          var urlsToCache = [
            './*',
            './Styles/*.css',
            './Scripts/*.js',
            './Fonts/*',
            './Images/*',
          ];
          cache.addAll(urlsToCache);
          console.log('cached');
        });
      })
    );
  }
});

self.addEventListener('fetch', function(event) {
  if (doCache) {
    event.respondWith(
      caches.match(event.request).then(function(response) {
        return response || fetch(event.request);
      })
    );
  }
});